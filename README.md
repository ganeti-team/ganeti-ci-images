# aptly-buster

An Debian Buster (10) with aptly to create gitlab-pages as repo.

the script /usr/bin/aptly-ci take a name as parameter and will import all *.deb with in the /name/*.deb directory and publish it in ./public

```gitlab-ci
stages:
  - ...
  - deploy
pages:
  stage: deploy
  image: registry...aptly-buster
  script:
  - /usr/bin/aptly-ci buster
  artifacts:
    paths:
    - ./public
  only:
  - master

```
